# Microfrontends with Single Spa and React

### Main Learnings

- Kinds of Single SPA instances

**single-spa application / parcel**: complete application or a component (parcel)

**in-browser utility module (styleguide, api cache, etc)**: share common content, styles, functions, etc... between the microfrontends

**single-spa root config**: main project, that link all other **single-spa application / parcel**

- Output and input values between microfrontends
R: emiting and listening a event

```
export function emitEvent(name, data) {
  dispatchEvent(new CustomEvent(name, {
    detail: data,
  }))
}

export function listenEvent(name, cb) {
  window.addEventListener(name, cb)
}
```

- Import a microfrontend
R: using Parcel 
```
import Parcel from 'single-spa-react/parcel'

<Parcel
config={() => System.import('@mc/react-parcel')}
/>
```

### Main commands

- Create a single spa ```npx create-single-spa```
- Start microfrontend at port XXXX ```npm start -- --port 8500```
- Single SPA Playground ```http://single-spa-playground.org/playground/instant-test?name=@[company-name]/[project-name]&url=[port]```

Reference video [link](https://www.youtube.com/watch?v=68LaXOWwxZI)

###  Video Summary

**1:45** Apresentando o single-spa

**5:26** Criando o orquestrador

**8:55** Explicando o index.ejs

**12:40** Explicando o mc-root-config.js

-------------------------------------------------

**13:45** Criando o micro frontend 1

**20:10** Adicionando dependências externas ao importmap

**22:25** Explicando o activeWhen

**24:25** Explicando o lifecycles com single-spa-react

-------------------------------------------------

**26:55** Criando o micro frontend 2: rotas internas

**29:20** Criando rotas com o react-router-dom

**31:20** Definindo o basename no BrowserRouter

-------------------------------------------------

**33:50** Criando o micro frontend 3: usando um parcel dentro de um micro frontend

**40:09** Explicando o Parcel

**42:15** Configurando a comunicação entre o micro frontend e o parcel com CustomEvents

**46:38** Recapitulando

**49:00** Criando o utility module que compartilha funções de comunicação entre micro frontends e parcels

**51:55** Explicando o mc-utils.js

**54:45** Explicando lazy loading e code splitting

-------------------------------------------------

**56:20** Criando o micro frontend 4: lazy loading e code splitting

**59:12** Aplicando lazy load às rotas

**1:00:00** Usando o Suspense

**1:01:20** Explicando o set-public-path.js

-------------------------------------------------

**1:02:09** Criando o micro frontend 5: barra de navegação compartilhada entre micro frontends

**1:05:20** Configurando a rota

**1:07:43** Explicando a configuração externals do webpack

**1:10:04** Adicionando o react-router-dom ao importmap para compartilhá-lo entre vários micro frontends

-------------------------------------------------

**1:11:48** Publicando tudo

**1:17:55** Hospedando o importmap na CDN e configurando CORS

**1:21:42** Hospedando micro frontends na CDN

**1:24:28** Testando os micro frontends hospedados

**1:25:15** Hospedando o orquestrador

**1:26:15** Explicando os benefícios

**1:26:40** Explicando como lidar com aplicações existentes

**1:27:37** Falando sobre outros frameworks
